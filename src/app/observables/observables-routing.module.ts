import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
  path: 'exo1',
  loadComponent: () => import('./exercises/exo1/exo1.component').then(m => m.Exo1Component)
  },
  {
    path: 'exo2',
    loadComponent: () => import('./exercises/exo2/exo2.component').then(m => m.Exo2Component)
  },
  {
    path: 'exo3',
    loadComponent: () => import('./exercises/exo3/exo3.component').then(m => m.Exo3Component)
  },
  {
    path: '**',
    redirectTo: 'exo1'
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObservablesRoutingModule { }
