import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonApiService} from "../../../ressources/services/person-api.service";
import {PersonDisplayComponent} from "../../../ressources/components/person-display/person-display.component";
import {MovieDisplayComponent} from "../../../ressources/components/movie-display/movie-display.component";

@Component({
  selector: 'app-exo4',
  standalone: true,
  imports: [CommonModule, PersonDisplayComponent, MovieDisplayComponent],
  template: `
    <section style="display: flex; flex-flow: column">
      <!-- TODO: Exo 4
        <app-person-display *ngFor="let person of (????)"></app-person-display>
      -->
    </section>
  `
})
export class Exo4Component {
  private personApiService: PersonApiService = inject(PersonApiService);

  // TODO: Exo 4
  // - Récupérer la liste de toutes les personnes de la base
  // - Filtrer les utilisateurs qui ont plus de 35 ans
  // - Les afficher
}
