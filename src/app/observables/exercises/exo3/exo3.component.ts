import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonApiService} from "../../../ressources/services/person-api.service";
import {PersonDisplayComponent} from "../../../ressources/components/person-display/person-display.component";
import {MovieDisplayComponent} from "../../../ressources/components/movie-display/movie-display.component";
import {MovieApiService} from "../../../ressources/services/movie-api.service";

@Component({
  selector: 'app-exo3',
  standalone: true,
  imports: [CommonModule, PersonDisplayComponent, MovieDisplayComponent],
  template: `
    <app-person-display></app-person-display>
    <app-movie-display></app-movie-display>
  `
})
export class Exo3Component {
  private personApiService: PersonApiService = inject(PersonApiService);
  private movieApiService: MovieApiService = inject(MovieApiService);

  protected readonly personIdToFind = '42';

  // TODO: Exo 3
  // - Récupérer une personne via le service d'API
  // - À partir de cette personne récupérer son film préféré via MovieApiService
  // - Afficher dans les composants
}
