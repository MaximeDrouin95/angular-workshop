import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonApiService} from "../../../ressources/services/person-api.service";
import {PersonDisplayComponent} from "../../../ressources/components/person-display/person-display.component";
import {MovieDisplayComponent} from "../../../ressources/components/movie-display/movie-display.component";
import {AgeDisplayComponent} from "../../../ressources/components/age-display/age-display.component";

@Component({
  selector: 'app-exo2',
  standalone: true,
  imports: [CommonModule, PersonDisplayComponent, MovieDisplayComponent, AgeDisplayComponent],
  template: `
    <app-age-display></app-age-display>
  `
})
export class Exo2Component {
  private personApiService: PersonApiService = inject(PersonApiService);

  protected readonly personIdToFind: number = 42;

  // TODO: Exo 2
  // - Récupérer une personne via le service d'API
  // - La transformer pour récupérer uniquement son âge
  // - Afficher dans le composant
}
