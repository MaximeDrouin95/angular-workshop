import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonApiService} from "../../../ressources/services/person-api.service";
import {PersonDisplayComponent} from "../../../ressources/components/person-display/person-display.component";

@Component({
  selector: 'app-exo1',
  standalone: true,
  imports: [CommonModule, PersonDisplayComponent],
  template: `
    <app-person-display></app-person-display>
  `
})
export class Exo1Component {
  private personApiService: PersonApiService = inject(PersonApiService);

  protected readonly personIdToFind: number = 42;

  // TODO: Exo 1
  // - Récupérer une personne via le service d'API
  // - Afficher avec app-person-display
}
