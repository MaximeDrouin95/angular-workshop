import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
  path: 'observables',
  loadChildren: () => import('./observables/observables.module').then(m => m.ObservablesModule)
  },
  {
    path: '**',
    redirectTo: 'observables'
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
