export interface Movie {
  id: number;
  title: string;
  length: number;
}
