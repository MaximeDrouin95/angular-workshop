import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Movie} from "../models/movie.model";

@Injectable({
  providedIn: 'root'
})
export class MovieApiService {
  constructor(@Inject('BASE_API_URL') private baseApiUrl: string,
              private http: HttpClient) {}

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.baseApiUrl}/movie.json`)
  }

  getMovieById(movieId: number): Observable<Movie> {
    return this.getMovies().pipe(map((movieList: Movie[]) => {
      const movie = movieList.find((movie) => movie.id === movieId);
      if (!movie) throw new HttpErrorResponse({
        status: 404,
        statusText: 'movie not found'
      })
      return movie;
    }))
  }
}
