import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Person} from "../models/person.model";

@Injectable({
  providedIn: 'root'
})
export class PersonApiService {
  constructor(@Inject('BASE_API_URL') private baseApiUrl: string,
              private http: HttpClient) {}

  getPersons(): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.baseApiUrl}/person.json`)
  }

  getPersonById(userId: number): Observable<Person> {
    return this.getPersons().pipe(map((userList: Person[]) => {
      const user = userList.find((user) => user.id === userId);
      if (!user) throw new HttpErrorResponse({
        status: 404,
        statusText: 'user not found'
      })
      return user;
    }))
  }
}
