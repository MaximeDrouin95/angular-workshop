import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Movie} from "../../models/movie.model";

@Component({
  selector: 'app-movie-display',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      Titre: {{data?.title || 'Inconnu'}}
    </p>
    <p>
      Durée: {{data?.length || 'Inconnue'}}
    </p>
  `,
  styles: [
  ]
})
export class MovieDisplayComponent {
  @Input() data: Movie | undefined;
}
