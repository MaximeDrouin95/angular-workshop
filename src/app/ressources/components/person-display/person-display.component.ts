import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Person} from "../../models/person.model";
import {AgeDisplayComponent} from "../age-display/age-display.component";

@Component({
  selector: 'app-person-display',
  standalone: true,
  imports: [CommonModule, AgeDisplayComponent],
  template: `
    <p>
      Nom: {{data?.name || 'Inconnu'}}
    </p>

    <app-age-display [data]="data?.age"></app-age-display>
  `,
  styles: [
  ]
})
export class PersonDisplayComponent {
  @Input() data: Person | undefined;
}
