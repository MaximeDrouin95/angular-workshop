import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-age-display',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      Age: {{data || 'Inconnu'}}
    </p>
  `,
  styles: [
  ]
})
export class AgeDisplayComponent {
  @Input() data: number | undefined;
}
